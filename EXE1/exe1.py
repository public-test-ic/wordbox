import sys

def convert_list(array):
    "Funtion to convert the list str to integers"
    return [int(i) for i in array]

def condition_range(value):
    "Funtion to check the range of N and M"
    if 1<=value<=10e5: return True
    return False

def happiness_function(info_input):

    input_split = info_input.split(sep='\\n')
    n, m = int(input_split[0].split(" ")[0]), int(input_split[0].split(" ")[1])

    try:
        # Condition of N
        if not condition_range(n):
            raise Exception("N out of range")
        #Confition of M
        if not condition_range(m):
            raise Exception("M out of range")
    except Exception as e:
        print(e)
        sys.exit()


    arrayInteger = convert_list(input_split[1].split())
    arrayA = convert_list(input_split[2].split())
    arrayB = convert_list(input_split[3].split())

    #Validation of happiness
    count_happiness = 0
    for i in arrayInteger:
        if i in arrayA: count_happiness+=1
        if i in arrayB: count_happiness-=1

    print("The Happiness as result is: {}".format(count_happiness))



if __name__ == "__main__":
    info_input = sys.argv[1]
    happiness_function(info_input)
