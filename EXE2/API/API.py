from flask import Flask, request, jsonify
from object_database.database_sqlite import *

#Object of database
database = Database("Users.db")
database.create_table()
app = Flask(__name__)


@app.route('/create_user', methods=['POST'])
def create_user():
    "Endpoint to create a user"
    if request.args.get('nombre') is not None:
        nombre = request.args.get('nombre')
    else:
        nombre = request.json['nombre']

    if request.args.get('apellidos') is not None:
        apellidos = request.args.get('apellidos')
    else:
        apellidos = request.json['apellidos']

    if request.args.get('direccion') is not None:
        direccion = request.args.get('direccion')
    else:
        direccion = request.json['direccion']

    if request.args.get('telefono') is not None:
        telefono = request.args.get('telefono')
    else:
        telefono = request.json['telefono']

    dict_values = {
    'nombre':nombre,
    'apellidos':apellidos,
    'direccion':direccion,
    'telefono':telefono
    }
    message = database.insert_row_table(dict_values)
    return message

@app.route('/get_user', methods=['GET'])
def get_user():
    "Endpoint to get a user"

    id_user = request.args.get('id_user')
    print(id_user)
    if id_user is  None:
        id_user = request.json['id_user']
    print(id_user)

    message = database.retrive_user_id(id_user)
    if len(message)==0: return "NO RESULTS FOUND"
    message = jsonify(message)
    return message

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=7000)
