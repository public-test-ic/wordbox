import sqlite3
import logging
import shortuuid
logging.basicConfig(format='%(levelname)s-%(message)s',  level=logging.INFO)

class Database(object):
    def __init__(self, name_db):
        """
        Class od SQLite
        """
        self.name_db = name_db
        self.conn = sqlite3.connect(self.name_db,check_same_thread=False)
        self.cur = self.conn.cursor()

    def create_table(self):
        try:
            self.conn.execute(
                """
                    create table users(
                        id_user text primary key,
                        nombre text NOT NULL,
                        apellidos text NOT NULL,
                        direccion text NOT NULL,
                        telefono text NOT NULL

                    )

                """
            )
            logging.info('Database created')
        except sqlite3.OperationalError:
            logging.warning("The database exist")

    def insert_row_table(self, dict_values):
        id_user = str(shortuuid.uuid())
        try:
            self.conn.execute(
                    """
                        insert into users(
                            id_user,
                            nombre,
                            apellidos,
                            direccion,
                            telefono
                        )
                        values(?,?,?,?,?)""",\
                        (id_user,  dict_values["nombre"], \
                        dict_values["apellidos"], \
                        dict_values["direccion"],dict_values["telefono"]))
            self.conn.commit()
            logging.info('Row inserted')
            return "id_user: "+id_user
        except Exception as e:
            return "ERROR creating the user:"+str(e)

    def retrive_user_id(self, id_user, table="users"):
        query = "SELECT * FROM " + table + " WHERE id_user ='"+id_user+"'"
        return self.cur.execute(query).fetchall()

if __name__ == "__main__":
    db = Database("./test.db")
    db.create_inference_table()
    dict_values = {
        "id_user": "5",
        "nombre": "wIvwaqn12",
        "apellidos": "chevrolet",
        "direccion": "rojo",
        "telefono": "Derechi",
    }
    db.insert_row_inference_table(dict_values)
