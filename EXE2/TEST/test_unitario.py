import sys
import json
import settings as stg
import requests
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--test', type=int, help="1 : create user\n2 : get user", required=True)
parser.add_argument('--id_user', type=str, help="id_user to consult", default="")


def create_user():
    json_data = {'nombre': stg.nombre,\
        'apellidos': stg.apellidos, 'direccion':stg.direccion,\
        'telefono':stg.telefono
            }
    api_url = stg.ip+'/create_user'
    r = requests.post(url=api_url, json=json_data)
    print("Estatus: {}, Message:{}".format(r.status_code, r.text))

def get_user(id_user):
    json_data = {'id_user': id_user}
    r = requests.get(stg.ip+"/get_user",params = json_data)
    print("Estatus: {}, Message:{}".format(r.status_code, r.text))

if __name__ == '__main__':
    args = parser.parse_args()
    if args.test == 1: create_user()
    else:get_user(args.id_user)
